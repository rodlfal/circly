package pt.ra.circly.utils;

public class CircleConfig {
	private int index;
	private float velocity;
	private float angle;
	private float areaAngle;
	private float areaVelocity;
	private float areaRotation;
	private float startRotation;
	
	public CircleConfig(){
		
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public float getVelocity() {
		return velocity;
	}

	public void setVelocity(float velocity) {
		this.velocity = velocity;
	}

	public float getAngle() {
		return angle;
	}

	public void setAngle(float angle) {
		this.angle = angle;
	}

	public float getAreaAngle() {
		return areaAngle;
	}

	public void setAreaAngle(float areaAngle) {
		this.areaAngle = areaAngle;
	}

	public float getAreaVelocity() {
		return areaVelocity;
	}

	public void setAreaVelocity(float areaVelocity) {
		this.areaVelocity = areaVelocity;
	}

	public float getAreaRotation() {
		return areaRotation;
	}

	public void setAreaRotation(float areaRotation) {
		this.areaRotation = areaRotation;
	}

	public float getStartRotation() {
		return startRotation;
	}

	public void setStartRotation(float startRotation) {
		this.startRotation = startRotation;
	}
	
	
}
