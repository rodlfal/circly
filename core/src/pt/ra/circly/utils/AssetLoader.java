package pt.ra.circly.utils;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;


public class AssetLoader {
	public static Texture background;
	public static TextureAtlas atlas;
	//public static BitmapFont centuryFont;
	//public static BitmapFont centuryFontBig;
	public static BitmapFont robotoFont30;
	//public static BitmapFont robotoCondensedFont30;
	public static BitmapFont robotoFont60;
	public static BitmapFont robotoFont200;
	public static Sound lostSound;
	public static Sound winSound;

	public static void load() {

		background = new Texture(Gdx.files.internal("textures/background.png"));
		atlas = new TextureAtlas(Gdx.files.internal("textures/textureatlas.txt"));
		//centuryFont = new BitmapFont(Gdx.files.internal("fonts/century.fnt"), Gdx.files.internal("fonts/century_0.png"), false);
		//centuryFontBig = new BitmapFont(Gdx.files.internal("fonts/century_big.fnt"), Gdx.files.internal("fonts/century_big_0.png"), false);

		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Roboto-Bold.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 45;
		robotoFont30 = generator.generateFont(parameter);
		parameter.size = 80;
		robotoFont60 = generator.generateFont(parameter);
		parameter.size = 200;
		robotoFont200 = generator.generateFont(parameter);
		generator.dispose();

		background.setWrap(Texture.TextureWrap.Repeat,Texture.TextureWrap.Repeat);

		lostSound = Gdx.audio.newSound(Gdx.files.internal("lostfx.wav"));
		winSound = Gdx.audio.newSound(Gdx.files.internal("winfx.ogg"));
	}
	
	public static TextureRegion getTexture(String name){
		
		return atlas.findRegion(name);
	}
	
	public static void dispose() {
		atlas.dispose();
		robotoFont30.dispose();
		robotoFont60.dispose();
		robotoFont200.dispose();
    }
}