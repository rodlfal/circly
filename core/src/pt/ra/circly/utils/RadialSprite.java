package pt.ra.circly.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;


public class RadialSprite{

	private Texture texture;
	
	private float angle, rotation, rotationOffset; 
	private float  width, height, u1, u2, v1, v2, du, dv;
	private Vector2 position, origin, scale;
	
	private Color color;
	private float[] vertices;
	private int nVertices;

	public RadialSprite(final TextureRegion textureRegion) {
		
		this.vertices = new float[16 * 5];

		this.texture = textureRegion.getTexture();
		this.u1 = textureRegion.getU();
		this.v1 = textureRegion.getV();
		this.u2 = textureRegion.getU2();
		this.v2 = textureRegion.getV2();
		this.du = u2 - u1;
		this.dv = v2 - v1;
		this.width = textureRegion.getRegionWidth();
		this.height = textureRegion.getRegionHeight();

		this.rotation = 0;
		this.rotationOffset = 0;


		setColor(Color.WHITE);	
	}
	
	
	
	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public Vector2 getPosition() {
		return position;
	}

	public void setPosition(Vector2 position) {
		this.position = position;
	}

	public Vector2 getOrigin() {
		return origin;
	}

	public void setOrigin(Vector2 origin) {
		this.origin = origin;
	}
	
	public Vector2 getScale() {
		return scale;
	}

	public void setScale(Vector2 scale) {
		this.scale = scale;
	}

	public float getAngle() {
		return angle;
	}

	public void setAngle(float angle) {
		this.angle = angle;
		if(this.angle < 0)
			this.angle = 360;
		else if(this.angle > 360)
			this.angle = 0;
	}

	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color){
		this.color = color;
	}

	public float getRotation() {
		return rotation;
	}
	
	public float getAbsRotation() {
		return rotation + this.rotationOffset;
	}

	public void setRotation(float rotation) {
		this.rotation = rotation;
	}

	public float getRotationOffset() {
		return rotationOffset;
	}

	public void setRotationOffset(float rotationOffset) {
		this.rotationOffset = rotationOffset;
	}

	private void setVert(int index, float x, float y, float c, float u, float v){
		this.vertices[(index * 5)] = x;
		this.vertices[(index * 5) + 1] = y;
		this.vertices[(index * 5) + 2] = c;
		this.vertices[(index * 5) + 3] = u;
		this.vertices[(index * 5) + 4] = v;
		
		this.nVertices += 5;

		//Gdx.app.log("RadialSprite", "vertice " + index + " x:" + x+ " y:" + y + " u:" + u + " v:" + v);
	}

	//TODO: improve fucntion
	private void createTriangles() {
		
		this.nVertices = 0;
		final float c = this.color.toFloatBits();
		
		final float width = this.width * scale.x;
		final float height = this.height * scale.y;
		final float x = this.position.x - (this.origin.x * this.scale.x);
        final float y = this.position.y - (this.origin.y * this.scale.y);
        final float uCenter = this.u1 + this.du / 2;
        final float vCenter = this.v1 + this.dv / 2;
        final float xCenter = x + width / 2;
        final float yCenter = y + height / 2;


		float angle1 = this.rotation - this.angle / 2;
		float angle2 = this.rotation + this.angle / 2;

		if(angle1 < 0)
			angle1 += 360;
		if(angle2 > 360)
			angle2 -= 360;

		ArrayList<Float> tempAngles = new ArrayList<Float>();
		float anglePoints[] = {45, 135, 225, 315};

		//Maybe increment atemp by 90 and then check if anglePoints[i] < atemp AND anglePoints[i] - 90 > atemp
		tempAngles.add(angle1);
		float atemp = angle1;
		for(int a = 0; a < (int)this.angle; a++) {
			for (int i = 0; i < anglePoints.length; i++) {
				if(Math.floor(atemp) == anglePoints[i])
					tempAngles.add(anglePoints[i]);
			}
			atemp = atemp + 1;
			if(atemp > 360)
				atemp = 0;
		}
		tempAngles.add(angle2);

		ArrayList<Vector2> tempVertices = new ArrayList<Vector2>();
		ArrayList<Vector2> tempUV = new ArrayList<Vector2>();
		for(Float ta:tempAngles){
			float tempTa = ta;
			if(tempTa == 360)
				tempTa = 0;

			float tempX = 0;
			float tempY = 0;
			float tempU = 0;
			float tempV = 0;
			//1 and 3

			double tan = Math.tan(Math.toRadians(tempTa));

			if(((tempTa <= 45 && tempTa >= 0) || tempTa > 315)){
				tempX = (float) xCenter + width/2;
				tempY = (float) (width / 2 * tan) + yCenter;

				tempU = (float)uCenter + this.du / 2;
				tempV = (float) (this.dv / 2 * tan) + vCenter;
			}else if((tempTa >= 135 && tempTa < 225)){
				tempX = (float) xCenter - width/2;
				tempY = (float) (width / 2 * -tan) + yCenter;

				tempU = (float) uCenter - this.du / 2;
				tempV = (float) (this.dv / 2 * -tan) + vCenter;
			}
			//2 and 4
			else if((tempTa > 45 && tempTa < 135)){
				if(tempTa != 90) {
					tempX = (float) (xCenter + (height / (2 * tan)));
					tempU = (float) (uCenter + (this.du / (2 * tan)));
				}
				else{
					tempX = xCenter;
					tempU = uCenter;
				}

				tempY = (float)  yCenter + height/2;
				tempV = (float) vCenter + this.dv / 2;
			}else if((tempTa >= 225 && tempTa <= 315)){
				if(tempTa != 270) {
					tempX = (float) (xCenter + (height / (2 * -tan)));
					tempU = (float) (uCenter + (this.du / (2 * -tan)));
				}else{
					tempX = xCenter;
					tempU = uCenter;
				}

				tempY = (float)  yCenter - height/2;
				tempV = (float) vCenter - this.dv / 2;
			}

			Vector2 v = new Vector2(tempX, tempY);
			tempVertices.add(v);
			Vector2 uv = new Vector2(tempU, tempV);
			tempUV.add(uv);
		}

		//create triangles, maybe this can be done in the top loop
		int n = 0;
		for(int i = 0; i < tempVertices.size(); i++){
			if(n%4 == 0){
				this.setVert(n, xCenter, yCenter, c, uCenter, vCenter);
				n++;

				if(i>0) {
					this.setVert(n, tempVertices.get(i - 1).x, tempVertices.get(i - 1).y, c, tempUV.get(i - 1).x, tempUV.get(i - 1).y);
					n++;
				}
			}

			this.setVert(n, tempVertices.get(i).x, tempVertices.get(i).y, c, tempUV.get(i).x, tempUV.get(i).y);
			n++;

		}
		this.setVert(n, xCenter, yCenter, c, uCenter, vCenter);

		//OLD CODE, this can only be rotated using camera matrix
        /*if(this.angle <= 180){
        	
        	float angle1 = 45 - this.angle / 2;
        	float angle2 = 45 + this.angle / 2;
        	
            float x11 = (float) x + width;
            float y11 = (float) (height / 2 * Math.tan(Math.toRadians(angle1))) + yCenter;
            float v11 = (float) (this.dv / 2 * Math.tan(Math.toRadians(angle1))) + vCenter;
            
            float x12 = (float) (xCenter + width / (2 * Math.tan(Math.toRadians(angle2))));
            float y12 = (float) y + height;
            float u12 = (float) (uCenter + this.du / (2 * Math.tan(Math.toRadians(angle2))));
        	
            //Gdx.app.log("RadialSprite", "x12: " + x12);

        	this.setVert(0, xCenter, yCenter, c, uCenter, vCenter);
        	this.setVert(1, x12, y12, c, u12, v2);
        	this.setVert(2, x + width, y + height, c, u2, v2);
        	this.setVert(3, x11, y11, c, u2, v11);
        	this.setVert(4, xCenter, yCenter, c, uCenter, vCenter);
        	
        }
        else if(this.angle <= 360){
        	
        	float angle1 = 135 - this.angle / 2;
        	float angle2 = this.angle / 2 - 45;
        	
            float y11 = (float) (height / 2 * Math.tan(Math.toRadians(angle1))) + yCenter;
            float v11 = (float) (this.dv / 2 * Math.tan(Math.toRadians(angle1))) + vCenter;
            float x12 = (float) (xCenter + width / (2 * Math.tan(Math.toRadians(angle2))));
            float u12 = (float) (uCenter + this.du / (2 * Math.tan(Math.toRadians(angle2))));
        	
            //Gdx.app.log("RadialSprite", "x12: " + x12);
        	
        	this.setVert(0, xCenter, yCenter, c, uCenter, vCenter);
        	this.setVert(1, x, y11, c, u1, v11);
        	this.setVert(2, x, y + height, c, u1, v2);
        	this.setVert(3, x + width, y + height, c, u2, v2);
        	this.setVert(4, xCenter, yCenter, c, uCenter, vCenter);
        	this.setVert(5, x + width, y + height, c, u2, v2);
        	this.setVert(6, x + width, y, c, u2, v1);
        	this.setVert(7, x12, y, c, u12, v1);
        	this.setVert(8, xCenter, yCenter, c, uCenter, vCenter);
        	
        }*/
                     
 	}
	
	public void updateVerts(){
		createTriangles();
	}
	
	public void render(final SpriteBatch batch, OrthographicCamera camera){
		//camera.up.set(0, 1, 0);
		//camera.direction.set(0, 0, -1);

		//camera.position.set(this.position.x, this.position.y ,0);
		//Gdx.app.log("RADIAL SPRITE", "" + (this.rotation + this.rotationOffset));
		//camera.rotateAround(new Vector3(this.position.x, this.position.y, 0), new Vector3(0,0,1) ,this.rotation + this.rotationOffset);
		//camera.rotate(this.rotation + this.rotationOffset);
		//camera.update();
		//batch.setProjectionMatrix(camera.combined);

		batch.setColor(this.color);
		batch.draw(texture, this.vertices, 0, this.nVertices);
		batch.flush();
	}
}
