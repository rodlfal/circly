package pt.ra.circly.utils;

import java.util.ArrayList;
import java.util.List;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

public class ConfigLoader {
	
	public static ArrayList<LevelConfig> loadConfig(){
		ArrayList<LevelConfig> configObjects = new ArrayList<LevelConfig>();
		
		Json json = new Json();
		try{
			ArrayList<JsonValue> jsonValueList = json.fromJson(ArrayList.class, Gdx.files.internal("levels.json"));
			
			for (JsonValue jo : jsonValueList) {
				configObjects.add(json.readValue(LevelConfig.class, jo));
			}
			
			
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
		
		return configObjects;
	}
}
