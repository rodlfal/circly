package pt.ra.circly.utils;

import com.badlogic.gdx.math.Vector2;

public class LevelConfig {
	
	private int time;
	private CircleConfig circleConfigs[];
	
	public LevelConfig(){
		
		
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public CircleConfig[] getCircleConfigs() {
		return circleConfigs;
	}

	public void setCircleConfigs(CircleConfig[] circleConfigs) {
		this.circleConfigs = circleConfigs;
	}
	
	
	
}