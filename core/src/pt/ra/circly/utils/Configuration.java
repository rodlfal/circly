package pt.ra.circly.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector3;

public class Configuration {
	public static String TITLE = "Circle Puzzle";
	public static int SCREEN_WIDTH = 800;
	public static int SCREEN_HEIGHT = 1024;
	public static int WORLD_WIDTH = 1000;
	public static int WORLD_HEIGHT = 1000;
	public static Color BACKGROUNB_COLOR = new Color(39f / 255f, 39f / 255f, 39f / 255f, 1);
	public static Color TEXT_COLOR = new Color(255f / 255f, 255f / 255f, 255f / 255f, 0.2f);
	public static Color MENU_TEXT_COLOR = new Color(215f / 255f, 215f / 255f, 215f / 255f, 1f);
	
	public static Color colors[] = {
		new Color(18 / 255f, 122 / 255f, 198 / 255f, 1),
		new Color(15 / 255f, 158 / 255f, 146 / 255f, 1),
		new Color(122 / 255f, 204 / 255f, 49 / 255f, 1),
		new Color(251 / 255f, 178 / 255f, 19 / 255f, 1),
		new Color(243 / 255f, 84 / 255f, 32 / 255f, 1),
		new Color(219 / 255f, 38 / 255f, 89 / 255f, 1),
		new Color(206 / 255f, 37 / 255f, 124 / 255f, 1),
		new Color(107 / 255f, 36 / 255f, 164 / 255f, 1),
	};

	public static Color animateToColors[] = {
			new Color(7 / 255f, 146 / 255f, 238 / 255f, 1),
			new Color(9 / 255f, 168 / 255f, 153 / 255f, 1),
			new Color(120 / 255f, 217 / 255f, 36 / 255f, 1),
			new Color(253 / 255f, 191 / 255f, 12 / 255f, 1),
			new Color(248 / 255f, 72 / 255f, 22 / 255f, 1),
			new Color(230 / 255f, 27 / 255f, 78 / 255f, 1),
			new Color(206 / 255f, 37 / 255f, 124 / 255f, 1),
			new Color(107 / 255f, 36 / 255f, 164 / 255f, 1),
	};
	
	/*public static float scales[] = {
		0.28f,
		0.38f,
		0.48f,
		0.58f,
		0.68f,
		0.78f,
		0.88f,
		0.98f
	};*/
	public static float multiplier = 1.2f;
	public static float centerButtonScale = 1.8f / multiplier;
	public static float targetAreaScale = 0.56f;
	public static float circlesScale = 2f / multiplier;
	public static float scales[] = {
			0.56f / multiplier ,
			0.76f / multiplier,
			0.96f / multiplier,
			1.16f / multiplier,
			1.36f / multiplier,
			1.56f / multiplier,
			1.96f / multiplier,
			2.16f / multiplier
	};
	
	public static String words[] = {
		"Start",
		"Next",
		"Lost",
		"Again"
	};
}