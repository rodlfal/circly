package pt.ra.circly;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import pt.ra.circly.screens.GameScreen;
import pt.ra.circly.screens.MenuScreen;
import pt.ra.circly.utils.AssetLoader;

public class Circly extends Game {

	private GameScreen gameScreen;
	private MenuScreen menuScreen;

	@Override
	public void create() {
		Gdx.app.log("Circly", "created");
		AssetLoader.load();

		this.menuScreen = new MenuScreen(this);
		this.gameScreen = new GameScreen(this);

		this.setGameScreen();

		Gdx.input.setCatchBackKey(true);
	}

	public void setGameScreen(){
		//this.menuScreen.pause();
		this.gameScreen.setHasInputProcessor();
		setScreen(this.gameScreen);
	}

	public void setMenuScreen(){
		//this.gameScreen.pause();
		//this.menuScreen = new MenuScreen(this);
		this.menuScreen.setHasInputProcessor();
		setScreen(this.menuScreen);
		//this.menuScreen.resume();
	}

	@Override
	public void dispose() {
		super.dispose();
		//AssetLoader.dispose();
	}
}
