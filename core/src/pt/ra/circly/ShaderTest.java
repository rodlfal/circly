package pt.ra.circly;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import pt.ra.circly.shaders.BlurShader;
import pt.ra.circly.shaders.BlurShader2;
import pt.ra.circly.utils.AssetLoader;
import pt.ra.circly.utils.Configuration;

/**
 * Created by rodlfal on 5/25/2016.
 */
public class ShaderTest implements ApplicationListener {

    private SpriteBatch batch;
    private BitmapFont font;

    private ExtendViewport viewport;
    private OrthographicCamera camera;

    private BlurShader2 blurShader;

    @Override
    public void create() {
        AssetLoader.load();

        this.camera = new OrthographicCamera();
        this.viewport = new ExtendViewport(Configuration.WORLD_WIDTH, Configuration.WORLD_HEIGHT, this.camera);
        //viewport.update(Gdx.app.getGraphics().getWidth(), Gdx.app.getGraphics().getHeight());
        viewport.apply();
        this.camera.position.set(0, 0 ,0);

        batch = new SpriteBatch();
        this.font = AssetLoader.robotoFont30;
        font.setColor(Color.RED);

        blurShader = new BlurShader2();
    }

    @Override
    public void dispose() {
        batch.dispose();
        font.dispose();
    }

    @Override
    public void render() {
        camera.update();
        Gdx.gl.glClearColor(Configuration.BACKGROUNB_COLOR.r, Configuration.BACKGROUNB_COLOR.g,
                Configuration.BACKGROUNB_COLOR.b, Configuration.BACKGROUNB_COLOR.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.combined);

        batch.begin();

        blurShader.begin(batch);

        font.draw(batch, "Hello World", 0, 0);

        blurShader.end(batch, camera);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {
        this.viewport.update(width, height);
        viewport.apply();
        this.camera.position.set(0, 0 ,0);
        camera.update();

        blurShader.resize(camera);
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }
}
