package pt.ra.circly.shaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.sun.prism.image.ViewPort;

import pt.ra.circly.game.GameRenderer;
import pt.ra.circly.utils.Configuration;

/**
 * Created by rodlfal on 5/24/2016.
 */
public class BlurShader2 {

    private ShaderProgram blurShader;
    private FrameBuffer blurTargetA, blurTargetB;
    private TextureRegion fboRegion;

    public static final int FBO_SIZE_W = 1024;
    public static final int FBO_SIZE_H = 1024;

    public static final float MAX_BLUR = 10f;
    public float blur = 2;

    public BlurShader2(){
        ShaderProgram.pedantic = false;

        blurShader = new ShaderProgram(VERT, FRAG);
        if (!blurShader.isCompiled()) {
            System.err.println(blurShader.getLog());
            System.exit(0);
        }
        if (blurShader.getLog().length()!=0)
            System.out.println(blurShader.getLog());

        //setup uniforms for our shader
        blurShader.begin();
        blurShader.setUniformf("dir", 0f, 0f);
        blurShader.setUniformf("resolution", FBO_SIZE_W * 2);
        blurShader.setUniformf("radius", 1f);
        blurShader.end();

    }

    public void setBlur(float blur){
        this.blur = blur;
        if(this.blur > MAX_BLUR)
            this.blur = MAX_BLUR;
    }

    public float getBlur(){
        return this.blur;
    }

    public void resize(Camera camera){
        blurTargetA = new FrameBuffer(Pixmap.Format.RGBA8888,
                (int)camera.viewportWidth,
                (int)camera.viewportHeight,
                false);
        blurTargetB = new FrameBuffer(Pixmap.Format.RGBA8888,
                (int)camera.viewportWidth,
                (int)camera.viewportHeight,
                false);

        fboRegion = new TextureRegion(blurTargetA.getColorBufferTexture());
        fboRegion.flip(false, true);
    }

    public void begin(SpriteBatch batch){
        blurTargetA.begin();
    }

    public void end(SpriteBatch batch, Camera cam){
        batch.flush();
        blurTargetA.end();

        batch.setShader(blurShader);
        blurShader.setUniformf("dir", 1f, 0f);
        blurShader.setUniformf("radius", this.blur);


        blurTargetB.begin();

        fboRegion = new TextureRegion(blurTargetA.getColorBufferTexture());
        fboRegion.flip(false, true);

        batch.draw(fboRegion, -cam.viewportWidth/2, -cam.viewportHeight/2);
        batch.flush();
        blurTargetB.end();

        blurShader.setUniformf("dir", 0f, 1f);
        blurShader.setUniformf("radius", this.blur);

        fboRegion = new TextureRegion(blurTargetB.getColorBufferTexture());
        fboRegion.flip(false, true);
        batch.draw(fboRegion, -cam.viewportWidth/2, -cam.viewportHeight/2);

        batch.setShader(null);
    }

    public void dispose(){
        blurShader.dispose();
    }

    final String VERT =
            "attribute vec4 "+ShaderProgram.POSITION_ATTRIBUTE+";\n" +
                    "attribute vec4 "+ShaderProgram.COLOR_ATTRIBUTE+";\n" +
                    "attribute vec2 "+ShaderProgram.TEXCOORD_ATTRIBUTE+"0;\n" +

                    "uniform mat4 u_projTrans;\n" +
                    " \n" +
                    "varying vec4 vColor;\n" +
                    "varying vec2 vTexCoord;\n" +

                    "void main() {\n" +
                    "	vColor = "+ShaderProgram.COLOR_ATTRIBUTE+";\n" +
                    "	vTexCoord = "+ShaderProgram.TEXCOORD_ATTRIBUTE+"0;\n" +
                    "	gl_Position =  u_projTrans * " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" +
                    "}";

    final String FRAG =
            "#ifdef GL_ES\n" +
                    "#define LOWP lowp\n" +
                    "precision mediump float;\n" +
                    "#else\n" +
                    "#define LOWP \n" +
                    "#endif\n" +
                    "varying LOWP vec4 vColor;\n" +
                    "varying vec2 vTexCoord;\n" +
                    "\n" +
                    "uniform sampler2D u_texture;\n" +
                    "uniform float resolution;\n" +
                    "uniform float radius;\n" +
                    "uniform vec2 dir;\n" +
                    "\n" +
                    "void main() {\n" +
                    "	vec4 sum = vec4(0.0);\n" +
                    "	vec2 tc = vTexCoord;\n" +
                    "	float blur = radius/resolution; \n" +
                    "    \n" +
                    "    float hstep = dir.x;\n" +
                    "    float vstep = dir.y;\n" +
                    "    \n" +
                    "	sum += texture2D(u_texture, vec2(tc.x - 4.0*blur*hstep, tc.y - 4.0*blur*vstep)) * 0.05;\n" +
                    "	sum += texture2D(u_texture, vec2(tc.x - 3.0*blur*hstep, tc.y - 3.0*blur*vstep)) * 0.09;\n" +
                    "	sum += texture2D(u_texture, vec2(tc.x - 2.0*blur*hstep, tc.y - 2.0*blur*vstep)) * 0.12;\n" +
                    "	sum += texture2D(u_texture, vec2(tc.x - 1.0*blur*hstep, tc.y - 1.0*blur*vstep)) * 0.15;\n" +
                    "	\n" +
                    "	sum += texture2D(u_texture, vec2(tc.x, tc.y)) * 0.16;\n" +
                    "	\n" +
                    "	sum += texture2D(u_texture, vec2(tc.x + 1.0*blur*hstep, tc.y + 1.0*blur*vstep)) * 0.15;\n" +
                    "	sum += texture2D(u_texture, vec2(tc.x + 2.0*blur*hstep, tc.y + 2.0*blur*vstep)) * 0.12;\n" +
                    "	sum += texture2D(u_texture, vec2(tc.x + 3.0*blur*hstep, tc.y + 3.0*blur*vstep)) * 0.09;\n" +
                    "	sum += texture2D(u_texture, vec2(tc.x + 4.0*blur*hstep, tc.y + 4.0*blur*vstep)) * 0.05;\n" +
                    "\n" +
                    "	gl_FragColor = vColor * vec4(sum.rgb, 1.0);\n" +
                    "}";
}
