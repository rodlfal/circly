package pt.ra.circly.shaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

/**
 * Created by rodlfal on 5/25/2016.
 */
public class BrightnessConstrastShader {

    private ShaderProgram shader;
    protected int brightnessLoc=-1, contrastLoc=-1;

    //ideally use getters/setters here...
    public float brightness=0.2f;
    public float contrast=1.6f;

    public BrightnessConstrastShader(){
        ShaderProgram.pedantic = false;
        shader = new ShaderProgram(vertexShader, fragmentShader);
        final boolean isCompiled = shader.isCompiled();

        if (!isCompiled) {
            Gdx.app.log("BCShader", shader.getLog());
            System.exit(0);
        }
        if (shader.getLog().length()!=0)
            Gdx.app.log("BCShader", shader.getLog());

        if (isCompiled) {
            shader.begin();
            brightnessLoc = shader.getUniformLocation("brightness");
            contrastLoc = shader.getUniformLocation("contrast");
            shader.end();
        }
    }

    public void begin(SpriteBatch batch){
        batch.setShader(shader);

        if (brightnessLoc!=-1 && shader!=null)
            shader.setUniformf(brightnessLoc, brightness);
        if (contrastLoc!=-1 && shader!=null)
            shader.setUniformf(contrastLoc, contrast);

    }


    public void end(SpriteBatch batch){
        batch.flush();
        batch.setShader(null);
    }

    static final String vertexShader = "attribute vec4 " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
            + "attribute vec4 " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
            + "attribute vec2 " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
            + "uniform mat4 u_projTrans;\n" //
            + "varying vec4 v_color;\n" //
            + "varying vec2 v_texCoords;\n" //
            + "\n" //
            + "void main()\n" //
            + "{\n" //
            + "   v_color = " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
            + "   v_texCoords = " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
            + "   gl_Position =  u_projTrans * " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
            + "}\n";

    static final String fragmentShader = "#ifdef GL_ES\n" //
            + "#define LOWP lowp\n" //
            + "precision mediump float;\n" //
            + "#else\n" //
            + "#define LOWP \n" //
            + "#endif\n" //
            + "varying LOWP vec4 v_color;\n" //
            + "varying vec2 v_texCoords;\n" //
            + "uniform sampler2D u_texture;\n" //
            + "uniform float brightness;\n" //
            + "uniform float contrast;\n" //
            + "void main()\n"//
            + "{\n" //
            + "  vec4 color = v_color * texture2D(u_texture, v_texCoords);\n"
            + "  color.rgb /= color.a;\n" //ignore alpha
            + "  color.rgb = ((color.rgb - 0.5) * max(contrast, 0.0)) + 0.5;\n" //apply contrast
            + "  color.rgb += brightness;\n" //apply brightness
            + "  color.rgb *= color.a;\n" //return alpha
            + "  gl_FragColor = color;\n"
            + "}";
}
