package pt.ra.circly.game;

import java.util.ArrayList;
import java.util.List;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import pt.ra.circly.game.listeners.CenterButtonListener;
import pt.ra.circly.game.listeners.CircleAnimationListener;
import pt.ra.circly.game.listeners.MenuListener;
import pt.ra.circly.utils.AssetLoader;
import pt.ra.circly.utils.CircleConfig;
import pt.ra.circly.utils.ConfigLoader;
import pt.ra.circly.utils.Configuration;
import pt.ra.circly.utils.LevelConfig;


public class GameWorld implements CenterButtonListener, CircleAnimationListener, MenuListener{
	
	private GameRenderer gameRenderer;
	private float runtime;

	private Menu menu;
	private Circle[] circles;
	private CenterButton centerButton;

	private CircleConfig circleConfigs[];
	private int currentCircleConfig;
	private int currentCircle;
	
	private ArrayList<LevelConfig> levelsConfig;
	private int level = -1;
	private float time = 0;
	private boolean hasCounter = false;
	
	private FPSLogger fpsLogger = new FPSLogger();
	

	private int manyAnimating = 0;
	private State state = State.initiating;
    private State lastState = State.initiating;
	
	private Hud hud;

	private Sound lostSound;
	private Sound winSound;

	public GameWorld(){
		this.runtime = 0;
		this.gameRenderer = new GameRenderer(this);
		this.hud = new Hud(this.gameRenderer);
	
		final Vector2 center = new Vector2(0, 0);
		this.centerButton = new CenterButton(center, new Color(Configuration.colors[0]), Configuration.centerButtonScale, this);
		this.centerButton.setText("");

		circles = new Circle[6];
		
		circles[0] = new Circle(AssetLoader.getTexture("1"), center, new Color(Configuration.colors[0]), Configuration.circlesScale, this);
		circles[1] = new Circle(AssetLoader.getTexture("2"), center, new Color(Configuration.colors[1]), Configuration.circlesScale, this);
		circles[2] = new Circle(AssetLoader.getTexture("3"), center, new Color(Configuration.colors[2]), Configuration.circlesScale, this);
		circles[3] = new Circle(AssetLoader.getTexture("4"), center, new Color(Configuration.colors[3]), Configuration.circlesScale, this);
		circles[4] = new Circle(AssetLoader.getTexture("5"), center, new Color(Configuration.colors[4]), Configuration.circlesScale, this);
		circles[5] = new Circle(AssetLoader.getTexture("6"), center, new Color(Configuration.colors[5]), Configuration.circlesScale, this);
		//circles[6] = new Circle(AssetLoader.getTexture("7"), center, new Color(Configuration.colors[6]), Configuration.circlesScale, this);

		this.lostSound = AssetLoader.lostSound;
		this.winSound = AssetLoader.winSound;

		this.loadConfig();

        this.setLevel(this.level + 1);
        this.menu = new Menu(this);
        this.activateMenu();
	}

    private void setState(State state){
        this.lastState = this.state;
        this.state = state;
    }

	public Circle[] getCircles() {
		return circles;
	}
	
	public Hud getHud() {
		return this.hud;
	}

	public Menu getMenu() {
		return this.menu;
	}

	public CenterButton getCenterButton() {
		return this.centerButton;
	}
	
	/*
	 * Load configuration file levels.json
	 */
	public boolean loadConfig(){
		
		levelsConfig = ConfigLoader.loadConfig();
		
		return true;
	}
	
	/*
	 * Prepare new level according to configuration file to play
	 */
	public void setLevel(int level){
		Gdx.app.log("GAME", "SETTING LEVEL " + level);
		if(this.levelsConfig.size() <= level){
            Gdx.app.log("GAME", "END OF GAME");
            return;
        }

		this.level = level;
		this.currentCircleConfig = 0;
		
		final LevelConfig levelConfig = levelsConfig.get(this.level);
		this.circleConfigs = levelConfig.getCircleConfigs();


        int circleIndexes[] = new int[circleConfigs.length];
		for(int i = 0; i < circleConfigs.length; i++){
			final int circleIndex = circleConfigs[i].getIndex();
			circles[circleIndex].setActive(true);
			circles[circleIndex].setVelocity(circleConfigs[i].getVelocity());
			circles[circleIndex].animateVisibleAngle(circleConfigs[i].getAngle(), 250f);
			circles[circleIndex].animateRotation(circleConfigs[i].getStartRotation(), 350f);

            circleIndexes[i] = circleIndex;

			if(i == 0)
				this.currentCircle = circleIndex;
		}

        boolean isInLevel;
        for(int i = 0; i < circles.length; i++){
            isInLevel = false;
            for(int ci:circleIndexes){
                if(ci == i) {
                    isInLevel = true;
                    break;
                }
            }
            if(!isInLevel) {
                //circles[i].setActive(false);
                circles[i].setVelocity(0);
                circles[i].animateVisibleAngle(0, 250f);
            }
        }

		final CircleConfig circleConfig = this.circleConfigs[this.currentCircleConfig];
				
		this.centerButton.setText("");
		this.centerButton.animateTargetArea(circleConfig.getAreaAngle(),
				new Color(Configuration.colors[currentCircle]), circleConfig.getAreaRotation(), 
				Configuration.scales[currentCircle], circleConfig.getAreaVelocity());
		
		if(levelConfig.getTime() > 0){
			this.time = levelConfig.getTime();
			this.hasCounter = true;
			this.centerButton.setText(String.valueOf(this.time));
		}else{
			this.time = 0;
			this.hasCounter = false;
		}

        //Color bColor = new Color(Configuration.colors[currentCircle]);

        //this.gameRenderer.setBackgroundColor(bColor);

		this.hud.setLevelIndicator(String.valueOf(this.level + 1));
        this.setState(State.running);
	}
	
	public void stopCircles(){
		final LevelConfig levelConfig = levelsConfig.get(this.level);
		this.circleConfigs = levelConfig.getCircleConfigs();
		
		for(int i = 0; i < circleConfigs.length; i++){
			final int circleIndex = circleConfigs[i].getIndex();
			circles[circleIndex].setVelocity(0f);
		}
	}
	
	public void resize(int width, int height){
		this.gameRenderer.resize(width, height);
		this.hud.resize(width, height);
	}
	
	public void update(float delta){
		this.gameRenderer.update(delta);
        this.menu.update(delta);

		if(this.state == State.running){
			if(this.hasCounter){
				this.time -= delta;
				//LOOSE WHEN TIME REACH 0
				final int roundTime = Math.round(this.time);
				if(roundTime < 0)
					this.time = 0;
					
				this.centerButton.setText(String.valueOf(roundTime));
				if(this.time <= 0){
					this.lostLevel();
				}
			}
		}
		
		for(int i = 0; i < circles.length; i++)
			circles[i].update(delta);
		
		this.centerButton.update(delta);
	}
	
	public void render(float delta){
		
		this.runtime += delta;
		
		this.gameRenderer.render(this.runtime);
		
		//Gdx.app.log("FPS", "fps: " + (1 / delta));
	}

	@Override
	public void onPressed() {
		//Gdx.app.log("CENTER BUTTON", "pressed");
		/*if(this.state == State.stopped){
			
		}*/
		
		//WAIT FOR ANIMATION ENDING
		if(this.state == State.preparedForNewLevel){
			this.setLevel(this.level + 1);
			return;
		}else if(this.state == State.waitingForRestartLevel){
			this.setLevel(this.level);
			return;
		}
		
		if(this.state == State.running){
			final Circle cCircle = this.circles[currentCircle];

			boolean isInside = this.centerButton.checkIfInsideArea(cCircle);
			
			if(isInside){
				
				//Gdx.app.log("CENTER BUTTON", "circle in position");
				cCircle.setVelocity(0);
				//Color c = new Color(Configuration.animateToColors[currentCircle]);
				//cCircle.animateColor(c, 12f);

				this.winSound.play();
				if(this.currentCircleConfig < this.circleConfigs.length - 1){
					this.currentCircleConfig++;
				
					this.currentCircle = this.circleConfigs[this.currentCircleConfig].getIndex();
				
					
					final CircleConfig circleConfig = this.circleConfigs[this.currentCircleConfig];
					this.centerButton.animateTargetArea(circleConfig.getAreaAngle(), 
							new Color(Configuration.colors[currentCircle]), circleConfig.getAreaRotation(), 
							Configuration.scales[currentCircle], circleConfig.getAreaVelocity());
				}else{
				
					//TODO: SHOW LEVEL NUMBER
					this.wonLevel();
				}
				
				
			}
			else{
				//TODO: LOOSE RESTART LEVEL BUT WAIT FOR BUTTON TOUCH MAYBE STOP EVERY MOTION
				//TODO: IF YOU FAIL 3 TIMES IT CAN RETURN 1 LEVEL

				this.lostLevel();
			}
		}
	}
	
	private void wonLevel(){
        this.setState(State.waitingForNewLevel);
		this.centerButton.setText(Configuration.words[1]);
		
		for(int i = 0; i < this.circleConfigs.length; i++){
			this.circles[this.circleConfigs[i].getIndex()].animateVisibleAngle(360f, 200f);
			this.manyAnimating++;
		}
		
		this.centerButton.animateTargetArea(0f, null, 0f, 0f, 0f);
	}
	
	private void lostLevel(){
		Gdx.app.log("CENTER BUTTON", "LOOSE");

		//this.lostSound.play();
        Gdx.input.vibrate(200);
		this.state = State.waitingForRestartLevel;
		this.centerButton.setText(Configuration.words[3]);
		this.stopCircles();
	}

	@Override
	public void onAngleAnimationFinish(Circle circle) {

		if(this.state == State.waitingForNewLevel){
			this.manyAnimating--;
			if(this.manyAnimating == 0){
				if(this.levelsConfig.size() - 1 > this.level){

                    this.setState(State.preparedForNewLevel);
					
				}else{
                    this.setState(State.endOfGame);
				}
			}
		}
	}

	@Override
	public void onRotationAnimationFinish(Circle circle) {
		
	}

	@Override
	public void onScaleAnimationFinish(Circle circle) {
		//circle.animateScale(circle.getInitialScale(), 3f);
	}

	@Override
	public void onColorAnimationFinish(Circle circle) {
		Gdx.app.log("GAMEWORLD", "color animation event");
		//circle.animateColor(new Color(circle.getInitialColor()), 10f);
	}

    public void activateMenu(){
        if(this.state == State.inMenu)
            return;

        if(this.lastState != State.initiating)
            this.menu.changeMenu(2);

        this.gameRenderer.goToBlur(10f, 18f);
        this.menu.goToOpacity(1f, 3f);

        this.setState(State.inMenu);
    }

    public void deactivateMenu(){
        if(this.state != State.inMenu)
            return;

        //this.gameRenderer.setBlurActive(false);
        this.gameRenderer.goToBlur(0f, -18f);
        this.menu.goToOpacity(0f, -3f);

    }

    @Override
    public void onMenuItemClicked(String item) {

        if(item == "START"){
            this.deactivateMenu();

            this.setLevel(this.level);
        }else if(item == "RETURN"){
            this.deactivateMenu();

            this.setState(this.lastState);
        }else if(item == "CONTINUE"){
            this.deactivateMenu();

            this.setLevel(this.level);
        }else if(item == "START OVER"){
            this.deactivateMenu();

            this.setLevel(0);
        }else if(item == "EXIT"){
            Gdx.app.exit();
        }
    }


    public enum State{
        initiating,
		waitingForNewLevel,
		waitingForRestartLevel,
		preparedForNewLevel,
		running,
		endOfGame,
        inMenu
	}



	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        final Vector2 touchPosition = this.gameRenderer.getViewport().unproject(new Vector2(screenX, screenY));

        final boolean r = this.menu.touchDown(touchPosition);

        if(!r)
		    this.centerButton.touchDown(Math.round(touchPosition.x), Math.round(touchPosition.y), pointer, button);
		
		return false;
	}

    public boolean keyDown(int keycode) {
        if(keycode == Input.Keys.BACK || keycode == Input.Keys.ESCAPE){
            this.activateMenu();
        }

        return false;
    }
	
}
