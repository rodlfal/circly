package pt.ra.circly.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import pt.ra.circly.game.listeners.CircleAnimationListener;
import pt.ra.circly.utils.RadialSprite;

public class Circle {
	
	private TextureRegion textureRegion;
	private RadialSprite radialSprite;

	private boolean isActive;
	
	private float visibleAngle;
	private float scale;
	private float initialScale;
	private float rotation;
	private Color color;
	private Color initialColor;
	private Vector2 position;
	
	private float velocity;
	
	private boolean animateAngle = false;
	private float animateToAngle = 0;
	private float animateToAngleVelocity = 150f;
	private boolean animateScale = false;
	private float animateToScale = 0;
	private float animateToScaleVelocity = 100f;
	private boolean animateRotation = false;
	private float animateToRotation = 0;
	private float animateToRotationVelocity = 100f;
	private boolean animateColor = false;
	private Color animateToColor = Color.WHITE;
	private float animateToColorVelocity = 10f;
	private float animateToColorInterpolation = 0f;
	
	//direction -1 / 1
	private int animateAngleDirection = 0;
	private int animateScaleDirection = 0;
	private int animateRotationDirection = 0;
	
	private CircleAnimationListener animationListener;
	
	public Circle(TextureRegion textureRegion, Vector2 position, Color color, float scale, CircleAnimationListener listener){
		this.visibleAngle = 360;
		this.isActive = false;
		this.color = color;
		this.initialColor = new Color(this.color);
		this.scale = scale;
		this.initialScale = this.scale;

		this.animationListener = listener;
		
		this.textureRegion = textureRegion;
		
		this.radialSprite = new RadialSprite(textureRegion);
		this.radialSprite.setColor(color);
		this.radialSprite.setPosition(position);
		this.radialSprite.setOrigin(new Vector2(this.textureRegion.getRegionWidth() / 2, this.textureRegion.getRegionHeight() / 2));
		this.radialSprite.setScale(new Vector2(scale, scale));
		this.radialSprite.setRotationOffset(45);
		
		this.position = position;
	}
	
	public void setScale(float scale){
		this.scale = scale;
		this.radialSprite.setScale(new Vector2(scale, scale));
	}

	public float getScale(){
		return this.scale;
	}

	public Color getColor(){
		return this.color;
	}

	public Color getInitialColor(){
		return this.initialColor;
	}

	public float getInitialScale(){
		return this.initialScale;
	}
	
	public void setColor(Color color){
		this.radialSprite.setColor(color);
		this.color = color;
	}
	
	public float getVisibleAngle() {
		return visibleAngle;
	}

	public void setVisibleAngle(float visibleAngle) {
		if(visibleAngle > 360)
			visibleAngle = 0;
		else if(visibleAngle < 0)
			visibleAngle = 360;
		
		this.visibleAngle = visibleAngle;
		this.radialSprite.setAngle(visibleAngle);
	}

	public float getVelocity() {
		return velocity;
	}

	public void setVelocity(float velocity) {
		this.velocity = velocity;
	}
	
	public void setRotation(float rotation){
		if(rotation > 360){
			rotation = 0;
		}else if(rotation < 0){
			rotation = 360;
		}
		this.rotation = rotation;
		this.radialSprite.setRotation(rotation);
	}
	
	public float getRotation(){
		return this.radialSprite.getRotation();
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	public Vector2 getPosition() {
		return position;
	}

	public void setPosition(Vector2 position) {
		this.position = position;
	}

	/*
	 * Calculate if circle is inside circle
	 */
	public boolean checkIfInside(Circle circle){
		
		float angleDiff = Math.abs(this.getVisibleAngle() - circle.getVisibleAngle());
		
		if(this.radialSprite.getRotation() - (angleDiff / 2) < circle.getRotation() &&
				this.radialSprite.getRotation() + (angleDiff / 2) > circle.getRotation()){
			return true;
		}
		
		return false;
	}
	
	public void animateVisibleAngle(float angle, float velocity){
		if(angle == this.visibleAngle)
			return;
		
		this.animateToAngleVelocity = velocity;
		
		if(angle > 360){
			angle = 360;
		}
		else if(angle < 0){
			angle = 0;
		}
		
		this.animateToAngle = angle;
		this.animateAngle = true;
		
		if(this.visibleAngle < angle)
			this.animateAngleDirection = 1;
		else
			this.animateAngleDirection = -1;
		
	}
	
	public void animateScale(float scale, float velocity){
		if(scale == this.scale)
			return;
		
		this.animateToScaleVelocity = velocity;
		
		this.animateToScale = scale;
		this.animateScale = true;
		
		if(this.scale < scale)
			this.animateScaleDirection = 1;
		else
			this.animateScaleDirection = -1;
	}
	
	public void animateRotation(float rotation, float velocity){
		if(rotation == this.rotation)
			return;
		
		this.animateToRotationVelocity = velocity;
		
		this.animateToRotation = rotation;
		this.animateRotation = true;
		
		if(this.rotation < rotation)
			this.animateRotationDirection = 1;
		else
			this.animateRotationDirection = -1;
	}
	
	public void animateColor(Color color, float velocity){
		if(this.colorEqual(0.01f, this.color, color))
			return;
		
		this.animateToColorVelocity = velocity;
		
		this.animateToColor = color;
		this.animateColor = true;
		this.animateToColorInterpolation = 0f;
	}
	
	public void recalcVerts(){
		this.radialSprite.updateVerts();
	}
	
	public void update(float delta){
		if(!this.isActive)
			return;
		
		boolean update = false; 
		
		if(this.animateAngle){
			
			final float newAngle = this.getVisibleAngle() + this.animateToAngleVelocity * delta * this.animateAngleDirection;
			
			if(this.animateAngleDirection == 1 && newAngle > this.animateToAngle || 
					this.animateAngleDirection == -1 && newAngle < this.animateToAngle){
				this.animateAngle = false;
				this.setVisibleAngle(this.animateToAngle);
				
				if(this.animationListener != null)
					this.animationListener.onAngleAnimationFinish(this);
				
			}else{
				this.setVisibleAngle(newAngle);
			}
			//Gdx.app.log("CIRCLE", "new Angle: " + newAngle);
			update = true;
		}
		
		if(this.animateScale){
			final float newScale = this.scale + this.animateToScaleVelocity * delta * this.animateScaleDirection;

			if(this.animateScaleDirection == 1 && newScale > this.animateToScale || 
					this.animateScaleDirection == -1 && newScale < this.animateToScale){
				this.animateScale = false;
				this.setScale(this.animateToScale);
				
				if(this.animationListener != null)
					this.animationListener.onScaleAnimationFinish(this);
				
			}else{
				this.setScale(newScale);
			}
			
			update = true;
		}
		
		if(this.animateColor){
			this.animateToColorInterpolation = this.animateToColorVelocity * delta;

			if(this.colorEqual(0.01f, this.color, this.animateToColor)){
				Gdx.app.log("CIRCLE", "color animation finished");
				this.animateColor = false;
				this.setColor(new Color(this.animateToColor));
				
				if(this.animationListener != null)
					this.animationListener.onColorAnimationFinish(this);
				
			}else{
				this.color.lerp(this.animateToColor, this.animateToColorInterpolation);
			}
			
			update = true;
		}
		
		if(this.animateRotation){
			final float newRotation = this.rotation + animateToRotationVelocity * delta * this.animateRotationDirection;
			
			if(this.animateRotationDirection == 1 && newRotation > this.animateToRotation || 
					this.animateRotationDirection == -1 && newRotation < this.animateToRotation){
				this.animateRotation = false;
				this.setRotation(this.animateToRotation);
				
				if(this.animationListener != null)
					this.animationListener.onRotationAnimationFinish(this);
				
			}else{
				this.setRotation(newRotation);
			}

			update = true;
		}else{
			this.setRotation(this.getRotation() - this.velocity * delta);

			update = true;
		}
		
		if(update)
			this.recalcVerts();
	}
	
	public void render(SpriteBatch batch, OrthographicCamera camera, float runtime){
		if(!this.isActive)
			return;
		
		this.radialSprite.render(batch, camera);	

	}

	private boolean colorEqual(float interval, Color c1, Color c2){

		if((c2.r - interval <= c1.r && c2.r + interval >= c1.r) &&
				(c2.g - interval <= c1.g && c2.g + interval >= c1.g) &&
					(c2.b - interval <= c1.b && c2.b + interval >= c1.b) &&
						(c2.a - interval <= c1.a && c2.a + interval >= c1.a))
			return true;

		return false;
	}
	
}
