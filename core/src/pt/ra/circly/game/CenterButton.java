package pt.ra.circly.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import pt.ra.circly.game.listeners.CenterButtonListener;
import pt.ra.circly.game.listeners.CircleAnimationListener;
import pt.ra.circly.utils.AssetLoader;
import pt.ra.circly.utils.Configuration;
import pt.ra.circly.utils.RadialSprite;

public class CenterButton implements CircleAnimationListener{
	
	private TextureRegion textureRegion;
	private TextureRegion textureRegionPressed;
	private CenterButtonListener centerButtonListener;
	
	private boolean pressed = false;
	
	private Color color;
	private Vector2 position;
	private Vector2 origin;
	private float scale;
	private float width;
	private float height;
	
	private Circle targetArea;
	private Circle button;
	
	private GlyphLayout layout = new GlyphLayout();
	private BitmapFont font;
	private String text = "";

	//private float initialScale;

	public CenterButton(Vector2 position, Color color, float scale, CenterButtonListener centerButtonListener) {
		this.color = color;
		this.position = position;
		this.scale = scale;
		//this.initialScale = this.scale;
		this.centerButtonListener = centerButtonListener;
		
		//Get textures for button states
		this.textureRegion = AssetLoader.getTexture("button");
		this.textureRegionPressed = AssetLoader.getTexture("button");
		
		this.width = this.textureRegion.getRegionWidth();
		this.height = this.textureRegion.getRegionHeight();
		this.origin = new Vector2(width/2, height/2);

		//Get texture for target area
		final TextureRegion areaTextureRegion = AssetLoader.getTexture("area");
		final Color targetAreaColor = new Color(color);
		targetAreaColor.a = 0.2f;
		
		this.targetArea = new Circle(AssetLoader.getTexture("area"), position, targetAreaColor, Configuration.targetAreaScale, null);

		this.button = new Circle(this.textureRegion, position, this.color, this.scale, this);
		this.button.setRotation(0);
		this.button.setVisibleAngle(360);
		this.button.setActive(true);
		this.button.recalcVerts();
		
		//INITIATE FONT AND TEXT
		//this.font = AssetLoader.centuryFont;
		this.font = AssetLoader.robotoFont30;
	}

	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text.toUpperCase();
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
		this.button.setColor(color);
		
		final Color targetAreaColor = new Color(color);
		targetAreaColor.a = 0.2f;
		this.targetArea.setColor(targetAreaColor);
	}
	
	public void setTargetArea(float visibleAngle, Color color, float rotation, float scale){
		this.targetArea.setActive(true);
		this.targetArea.setVisibleAngle(visibleAngle);
		this.targetArea.setRotation(rotation);
		this.targetArea.setScale(scale);
		
		final Color targetAreaColor = new Color(color);
		targetAreaColor.a = 0.2f;
		this.targetArea.setColor(targetAreaColor);
		
		this.color = color;
		
		this.targetArea.recalcVerts();
	}
	
	public void animateTargetArea(float visibleAngle, Color color, float rotation, float scale, float velocity){
		this.targetArea.setActive(true);
		if(velocity == 0){
			this.targetArea.setVelocity(0);
			this.targetArea.animateRotation(rotation, 120f);
		}else{
			this.targetArea.setVelocity(velocity);
		}
			
		this.targetArea.animateVisibleAngle(visibleAngle, 250f);
		this.targetArea.animateScale(scale, 1.8f);
		
		if(color != null){

			final Color targetAreaColor = new Color(color);
			targetAreaColor.a = 0.2f;
			this.targetArea.animateColor(targetAreaColor, 5f);
			this.button.animateColor(color, 5f);
			this.color = color;
		}
	}
	
	/*
	 * Check if circle is inside target area
	 */
	public boolean checkIfInsideArea(Circle circle){	
		return this.targetArea.checkIfInside(circle);
	}
	
	public void update(float delta){
		this.targetArea.update(delta);
		this.button.update(delta);
	}
	
	public void render(final SpriteBatch batch, OrthographicCamera camera, float runtime){

		this.targetArea.render(batch, camera, runtime);
		this.button.render(batch, camera, runtime);


		font.setColor(Configuration.BACKGROUNB_COLOR);
		layout.setText(font, this.text);
		font.draw(batch, layout, this.position.x - layout.width / 2, this.position.y + layout.height / 2);
	}
	
	//MANAGE BUTTON TOUCHES
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		
		Gdx.app.log("POSITION ", "x: " + screenX + " y: " + screenY );

		
		int dx =  Math.abs(screenX - Math.round(this.position.x));
		int dy =  Math.abs(screenY - Math.round(this.position.y));

		//verify if is inside button
		//if(dx + dy <= (this.width * this.scale) / 2){
		this.pressed = true;
		centerButtonListener.onPressed();
		this.pressed = false;

		this.button.animateScale(this.scale + 0.3f, 3);
		//}
		//Gdx.app.log("POSITION ", "x: " + this.position.x + " y: " + this.position.y );
		//Gdx.app.log("CENTER BUTTON", "x: " + screenX + " y: " + screenY + " dx: " + dx + " dy" + dy );
		
		return false;
	}

	@Override
	public void onAngleAnimationFinish(Circle circle) {

	}

	@Override
	public void onRotationAnimationFinish(Circle circle) {

	}

	@Override
	public void onScaleAnimationFinish(Circle circle) {
		this.button.animateScale(this.scale, 2);
	}

	@Override
	public void onColorAnimationFinish(Circle circle) {

	}
}
