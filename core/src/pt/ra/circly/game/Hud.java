package pt.ra.circly.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

import pt.ra.circly.utils.AssetLoader;
import pt.ra.circly.utils.Configuration;

public class Hud {
	private GlyphLayout layout = new GlyphLayout();
	private BitmapFont font;
	
	private Vector2 levelIndicatorPosition;
	private String levelIndicator = "";

	private GameRenderer gameRenderer;

	public Hud(GameRenderer gameRenderer){
		this.gameRenderer = gameRenderer;
		this.font = AssetLoader.robotoFont200;
        this.font.setColor(Configuration.TEXT_COLOR);
	}
	
	public void setLevelIndicator(String levelIndicator){
		this.levelIndicator = levelIndicator;
	}

	public void resize(int width, int height)
	{
		final Viewport viewport = gameRenderer.getViewport();
		//this.levelIndicatorPosition = new Vector2(0, Gdx.app.getGraphics().getHeight());
		this.levelIndicatorPosition = viewport.unproject(new Vector2(30, 30));
		Gdx.app.log("HUD", "" + this.levelIndicatorPosition);
	}

	public void render(SpriteBatch batch, OrthographicCamera camera, float runtime){

		font.setColor(Configuration.TEXT_COLOR);
		layout.setText(font, this.levelIndicator);;
		font.draw(batch, layout, this.levelIndicatorPosition.x, this.levelIndicatorPosition.y);
	}
}
