package pt.ra.circly.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import pt.ra.circly.Circly;
import pt.ra.circly.game.listeners.MenuListener;
import pt.ra.circly.utils.AssetLoader;
import pt.ra.circly.utils.Configuration;

/**
 * Created by rodlfal on 5/24/2016.
 */
public class Menu {
    private GlyphLayout layout = new GlyphLayout();
    private BitmapFont font;

    private String[][] menu = {{"START", "EXIT"}, {"CONTINUE", "START OVER", "EXIT"}, {"RETURN", "START OVER", "EXIT"}};
    private int nMenu = 0;

    private int buttonWidth = 400;
    private int buttonHeight = 120;
    private int padding = 60;
    private Vector2 menuPosition;
    private Vector2 menuSize;

    private TextureRegion backgroundRegion;
    private Color backgroundColor;
    private Color textColor;
    private boolean isActive = false;

    private MenuListener menuListener;


    private float opacity = 0;
    private boolean opacityAnimate = false;
    private float goToOpacity = 0;
    private float goToOpacityVelocity = 0;

    public Menu(MenuListener menuListener){
        this.font = AssetLoader.robotoFont60;

        this.changeMenu(0);
        menuPosition = new Vector2(0, 0);

        backgroundRegion = new TextureRegion(AssetLoader.background);
        backgroundColor = new Color(0f, 0f, 0f, 0.4f);
        textColor = Configuration.MENU_TEXT_COLOR;

        this.menuListener = menuListener;
    }

    public void changeMenu(int nMenu){
        this.nMenu = nMenu;

        menuSize = new Vector2(buttonWidth, (this.buttonHeight) * this.menu[this.nMenu].length);
    }

    public void setActive(boolean isActive){
        this.isActive = isActive;
    }

    public void setOpacity(float opacity){
        if(opacity > 1)
            this.opacity = 1;
        else if(opacity < 0)
            this.opacity = 0;
        else
            this.opacity = opacity;
    }

    public void goToOpacity(float toOpacity, float velocity){
        this.setActive(true);
        this.goToOpacity = toOpacity;
        this.goToOpacityVelocity = velocity;
        this.opacityAnimate = true;
    }

    public boolean touchDown(Vector2 touchPosition) {
        if(!isActive)
            return false;

        int x = (int)(this.menuPosition.x);
        int y = (int)(this.menuPosition.y + (this.menuSize.y / 2));

        for(String m : this.menu[nMenu])
        {
            if((touchPosition.x > x - this.buttonWidth / 2 && touchPosition.x < x + this.buttonWidth / 2)
                    && (touchPosition.y > y - this.buttonHeight / 2 && touchPosition.y < y + this.buttonHeight / 2)){

                this.menuListener.onMenuItemClicked(m);
            }

            y -= this.buttonHeight+padding;
        }

        return true;
    }

    public void update(float delta){
        if(this.opacityAnimate){

            this.setOpacity(this.opacity + this.goToOpacityVelocity * delta);

            if((this.goToOpacityVelocity < 0 && this.goToOpacity >= this.opacity) ||
                    (this.goToOpacityVelocity > 0 && this.goToOpacity <= this.opacity)){
                this.opacityAnimate = false;
                Gdx.app.log("Menu", "opacity " + this.opacity);

                if(this.opacity <= 0)
                    this.setActive(false);
            }
        }
    }

    public void render(SpriteBatch batch, Viewport viewport) {
        if(!isActive)
            return;

        int x = (int)(this.menuPosition.x);
        int y = (int)(this.menuPosition.y + (this.menuSize.y / 2));

        this.backgroundColor.a = this.opacity * 0.5f;
        this.textColor.a = this.opacity;

        batch.setColor(this.backgroundColor);
        batch.draw(this.backgroundRegion, -viewport.getWorldWidth() / 2, -viewport.getWorldHeight() / 2,
                viewport.getWorldWidth(), viewport.getWorldHeight());

        this.font.setColor(Configuration.MENU_TEXT_COLOR);
        for(String m : this.menu[this.nMenu])
        {
            layout.setText(font, m);
            font.draw(batch, layout, x - layout.width / 2, y + layout.height / 2);

            y -= this.buttonHeight + padding;
        }
    }
}
