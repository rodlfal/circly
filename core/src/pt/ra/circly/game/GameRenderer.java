package pt.ra.circly.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import pt.ra.circly.shaders.BlurShader;
import pt.ra.circly.shaders.BrightnessConstrastShader;
import pt.ra.circly.utils.Configuration;

public class GameRenderer {

	private GameWorld gameWorld;
	
	private ExtendViewport viewport;
	
	private OrthographicCamera camera;
	private SpriteBatch batch;

	private Color backgroundColor;

    private BlurShader blurShader;
    private BrightnessConstrastShader bcShader;
    private boolean blurActive = false;

	private boolean blurAnimate = false;
	private float goToBlur = 0;
	private float goToBlurVelocity = 0;

	public GameRenderer(GameWorld gameWorld){

		this.backgroundColor = Configuration.BACKGROUNB_COLOR;

		this.camera = new OrthographicCamera();
		this.viewport = new ExtendViewport(Configuration.WORLD_WIDTH, Configuration.WORLD_HEIGHT, this.camera);
		//viewport.update(Gdx.app.getGraphics().getWidth(), Gdx.app.getGraphics().getHeight());
		viewport.apply();
		this.camera.position.set(0, 0 ,0);

		this.batch = new SpriteBatch();
		this.gameWorld = gameWorld;

        this.blurShader = new BlurShader(this);
        this.bcShader = new BrightnessConstrastShader();
	}

	public void setBlurActive(boolean blurActive){
		this.blurActive = blurActive;
	}

	public void goToBlur(float toBlur, float velocity){
		this.blurActive = true;
        this.goToBlur = toBlur;
        this.goToBlurVelocity = velocity;
        this.blurAnimate = true;
	}
	
	public OrthographicCamera getCamera(){
		return this.camera;
	}
	
	public Viewport getViewport(){
		return this.viewport;
	}
	
	public void setCameraPosition(Vector2 position){
		this.camera.position.set(position.x, position.y, 0);
	}
	
	public void resize(int width, int height){
		this.viewport.update(width, height);
        viewport.apply();
        this.camera.position.set(0, 0 ,0);
        camera.update();

        this.blurShader.resize();
	}

	public void setBackgroundColor(Color backgroundColor) { this.backgroundColor = backgroundColor; }

	public Color getBackgroundColor() { return this.backgroundColor; }
    public BlurShader getBlurShader() { return this.blurShader; }

	void renderEntities(SpriteBatch batch, float runtime) {
		final Hud hud = this.gameWorld.getHud();

		final Circle[] circles = this.gameWorld.getCircles();
		final CenterButton centerButton = this.gameWorld.getCenterButton();

		hud.render(batch, camera, runtime);

        //this.bcShader.begin(batch);
		centerButton.render(batch, camera, runtime);
        //this.bcShader.end(batch);

		for(int i = 0; i < circles.length; i++)
			circles[i].render(batch, camera, runtime);



        batch.setColor(com.badlogic.gdx.graphics.Color.WHITE);
	}

	public void update(float delta){
		if(this.blurAnimate){

			float newBlur = this.blurShader.getBlur();

			newBlur += (this.goToBlurVelocity * delta);

			if((this.goToBlurVelocity < 0 && this.goToBlur >= this.blurShader.getBlur()) ||
					(this.goToBlurVelocity > 0 && this.goToBlur <= this.blurShader.getBlur())){
				this.blurAnimate = false;
                Gdx.app.log("GameRenderer", "bluranimate state" + this.blurAnimate);

                if(this.blurShader.getBlur() <= 0)
                    this.blurActive = false;
			}

			this.blurShader.setBlur(newBlur);
		}
	}

	public void render(float runtime){
		camera.update();
		Gdx.gl.glClearColor(this.backgroundColor.r, this.backgroundColor.g, this.backgroundColor.b, this.backgroundColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.setProjectionMatrix(camera.combined);

        if(!blurActive)
            batch.begin();
        else if(blurActive)
            this.blurShader.begin(batch, camera, this.backgroundColor);
        //Render with blur
		this.renderEntities(batch, runtime);

        if(blurActive)
            this.blurShader.end(batch, camera);

        //Render without blur
        final Menu menu = this.gameWorld.getMenu();
        menu.render(batch, this.viewport);

       	batch.end();
	}
}
