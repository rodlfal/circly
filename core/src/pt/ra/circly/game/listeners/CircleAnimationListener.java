package pt.ra.circly.game.listeners;

import pt.ra.circly.game.Circle;

public interface CircleAnimationListener {
	void onAngleAnimationFinish(Circle circle);
	void onRotationAnimationFinish(Circle circle);
	void onScaleAnimationFinish(Circle circle);
	void onColorAnimationFinish(Circle circle);
}
