package pt.ra.circly.game.listeners;

public interface MenuListener {
    public void onMenuItemClicked(String item);
}