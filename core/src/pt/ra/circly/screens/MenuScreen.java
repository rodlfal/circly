package pt.ra.circly.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import pt.ra.circly.Circly;
import pt.ra.circly.utils.AssetLoader;
import pt.ra.circly.utils.Configuration;

/**
 * Created by rodlfal on 5/21/2016.
 */
public class MenuScreen implements Screen, InputProcessor {

    private GlyphLayout layout = new GlyphLayout();
    private OrthographicCamera camera;
    private ExtendViewport viewport;
    private BitmapFont font;
    private SpriteBatch batch;
    private Color backgroundColor;

    private String[] menu = {"Start", "Continue", "Help", "Exit"};
    private int buttonWidth = 400;
    private int buttonHeight = 100;
    private int padding = 20;
    private Vector2 menuPosition;
    private Vector2 menuSize;

    private Circly circly;


    public MenuScreen(Circly circly){

        this.circly = circly;

        this.backgroundColor = Configuration.BACKGROUNB_COLOR;

        this.camera = new OrthographicCamera();
        this.viewport = new ExtendViewport(Configuration.WORLD_WIDTH, Configuration.WORLD_HEIGHT, this.camera);
        //viewport.update(Gdx.app.getGraphics().getWidth(), Gdx.app.getGraphics().getHeight());
        viewport.apply();
        this.camera.position.set(0, 0 ,0);

        this.batch = new SpriteBatch();
        this.batch.setProjectionMatrix(camera.combined);

        this.font = AssetLoader.robotoFont60;

        menuSize = new Vector2(buttonWidth, (this.buttonHeight) * this.menu.length);
        menuPosition = new Vector2(0, 0);

    }

    public void setHasInputProcessor(){
        Gdx.input.setInputProcessor(this);
    }


    @Override
    public void render(float delta) {
        //Gdx.app.log("MenuScreen", "Render");
        Gdx.gl.glClearColor(this.backgroundColor.r, this.backgroundColor.g, this.backgroundColor.b, this.backgroundColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.up.set(0, 1, 0);
        camera.direction.set(0, 0, -1);
        camera.update();
        batch.setProjectionMatrix(camera.combined);

        batch.begin();

        int x = (int)(this.menuPosition.x);
        int y = (int)(this.menuPosition.y + (this.menuSize.y / 2));

        this.font.setColor(Configuration.MENU_TEXT_COLOR);
        for(String m : this.menu)
        {
            //Gdx.app.log("MenuScreen", "Render " + m + x + y);
            layout.setText(font, m);
            font.draw(batch, layout, x - layout.width / 2, y + layout.height / 2);

            y -= this.buttonHeight + padding;
        }
        batch.end();

    }

    @Override
    public void show() {
        Gdx.app.log("MenuScreen", "show");
    }

    @Override
    public void resize(int width, int height){
        this.viewport.update(width, height);
    }

    @Override
    public void pause() {
        Gdx.app.log("MenuScreen", "pause called");
    }

    @Override
    public void resume() {
        Gdx.app.log("MenuScreen", "resume called");
    }

    @Override
    public void hide() {
        Gdx.app.log("MenuScreen", "hide called");
    }

    @Override
    public void dispose() {
        Gdx.app.log("MenuScreen", "dispose called");
    }


    public Viewport getViewport(){
        return this.viewport;
    }


    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if(keycode == Input.Keys.BACK){
            //Gdx.app.exit();
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        final Vector2 touchPosition = this.viewport.unproject(new Vector2(screenX, screenY));
        Gdx.app.log("MENU", "position " + touchPosition);
        //Check which menu was pressed
        int x = (int)(this.menuPosition.x);
        int y = (int)(this.menuPosition.y + (this.menuSize.y / 2));

        for(String m : this.menu)
        {
            if((touchPosition.x > x - this.buttonWidth / 2 && touchPosition.x < x + this.buttonWidth / 2)
                    && (touchPosition.y > y - this.buttonHeight / 2 && touchPosition.y < y + this.buttonHeight / 2)){

                Gdx.app.log("MENU", "option " + m);
                if(m == "Start"){
                    this.circly.setGameScreen();
                }
            }

            y -= this.buttonHeight+padding;
        }

        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
