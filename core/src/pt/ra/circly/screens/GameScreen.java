package pt.ra.circly.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.ui.Button;

import pt.ra.circly.Circly;
import pt.ra.circly.game.GameWorld;

public class GameScreen implements Screen, InputProcessor {

	private GameWorld gameWorld;
	private Circly circly;

	public GameScreen(Circly circly){
		Gdx.app.log("GameScreen", "Attached");
		this.circly = circly;
		
		gameWorld = new GameWorld(); // initialize world

		//Gdx.input.setInputProcessor(new InputHandler(gameWorld.getSpaceShip()));
	}

	public void setHasInputProcessor(){
		Gdx.input.setInputProcessor(this);
	}

	@Override
	public void show() {
		Gdx.app.log("GameScreen", "show");
	}


	@Override
	public void render(float delta) {
		//Gdx.app.log("GameScreen", "render " + delta);
		
		gameWorld.update(delta);
		gameWorld.render(delta);
	}

	@Override
	public void resize(int width, int height) {
		gameWorld.resize(width, height);
	}

	@Override
	public void pause() {
		Gdx.app.log("GameScreen", "pause called"); 
	}

	@Override
	public void resume() {
		Gdx.app.log("GameScreen", "resume called"); 
	}

	@Override
	public void hide() {
		Gdx.app.log("GameScreen", "hide called"); 
	}

	@Override
	public void dispose() {
		Gdx.app.log("GameScreen", "dispose called"); 
	}

	@Override
	public boolean keyDown(int keycode) {
		if(keycode == Input.Keys.BACK || keycode == Input.Keys.ESCAPE){
			//this.circly.setMenuScreen();
		}
		this.gameWorld.keyDown(keycode);

		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {

		this.gameWorld.touchDown(screenX, screenY, pointer, button);

		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
