#Circly

![1686587573-570529476-circle puzzle.jpg](https://bitbucket.org/repo/Mxz5GM/images/559079418-1686587573-570529476-circle%20puzzle.jpg)

Puzzle type game where the player need to stop the semicircles in the correct areas.
The central button should, stop the semi-circle, go to the next level or repeat the same level if the player loose the level.

There is 5 semi-circles maximum, each one has a different color defined in *utils.Configuration* class.



## Level Configuration File ##
There is one file for level configuration in Android project folder *assets* named *levels.json*. This is in a simple json format. It is an array of objects.

* **time** - is the max time the player has to finish the level, if 0 then there is no counting down.
* **circleConfigs** - array of objects too, each object represents configuration for different circle.
* * **index** - circle index [0 .. 7], 0 is the smaller one and 7 the bigger one.
* * **velocity** - rotation velocity of the semicircle.
* * **angle** - size of the semicircle. [0 .. 360]
* * **areaAngle** - size of the target area for the semicircle. [0 .. 360]
* * **areaVelocity** - rotation velocity of the target area. If 0 then is static. 
* * **areaRotation** - starting rotation for the target area.
* * **startRotation** - starting rotation for the semicircle.

The file is parsed and instances of *utils.LevelConfiguration* and *utils.CircleConfiguration* are created, so, to add more configurations, these two files should be updated with the new attributes too.



CHANGES:

Now, player can touch anywhere in the screen to stop semi-circles.