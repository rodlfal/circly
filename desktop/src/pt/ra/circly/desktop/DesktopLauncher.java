package pt.ra.circly.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import pt.ra.circly.Circly;
import pt.ra.circly.ShaderTest;
import pt.ra.circly.utils.Configuration;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = Configuration.TITLE;
		config.width = Configuration.SCREEN_WIDTH;
		config.height = Configuration.SCREEN_HEIGHT;

		new LwjglApplication(new Circly(), config);
		//new LwjglApplication(new ShaderTest(), config);
	}
}